﻿using UnityEngine;
using System.Collections;

public class DisactivatorVR : MonoBehaviour {

    public GameObject _cardboardCamera;
    public GameObject _oculusCamera;

    void Start() {


        if (IsUsingGearVR())
        {
            _oculusCamera.SetActive(true);
            _cardboardCamera.SetActive(false);
        }else {
            _oculusCamera.SetActive(false);
            _cardboardCamera.SetActive(true);
        }


    }
    bool IsUsingGearVR() {
        foreach (string device in UnityEngine.VR.VRSettings.supportedDevices)
        {
            print("Device used :" + device);
            if (device.ToLower().Contains("oculus"))
                return true;
        }
        return false;
        
    }
	
}
