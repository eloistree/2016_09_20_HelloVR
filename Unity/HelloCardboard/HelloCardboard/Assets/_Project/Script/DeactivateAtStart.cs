﻿using UnityEngine;
using System.Collections;

public class DeactivateAtStart : MonoBehaviour {

    void Start () {
        gameObject.SetActive(false);
	}
	
}
