﻿using UnityEngine;
using System.Collections;
using System;

public class SayMyHistory : MonoBehaviour {

    public string _userName;

    public AudioSource _sourceOfTheSound;
    public AudioClip _audioHistoryOfUser;
    public GameObject[] _visualObjectInformation;
    public TextMesh _nameOfUserAsText;
    

    public void DisplayTheUser(bool displayUser) {

      
            if(displayUser)
                 StartTalking();
            else
                StopTalkingAbout();
            DisplayInformation(displayUser);
            _nameOfUserAsText.text = _userName;
        
     
        
       

    }

    private void DisplayInformation(bool displayInformation)
    {
        for (int i = 0; i < _visualObjectInformation.Length; i++)
        {
            _visualObjectInformation[i].SetActive(displayInformation);
            
        }
    }

    public void StartTalking() {
        _sourceOfTheSound.clip = _audioHistoryOfUser;
        _sourceOfTheSound.Play();
    }
    public void StopTalkingAbout() {
        
        _sourceOfTheSound.Stop();
    }
}
